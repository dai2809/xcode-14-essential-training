//
//  Xcode_14_Essential_TrainingApp.swift
//  Xcode 14 Essential Training
//
//  Created by wyrd on 2023/07/26.
//

import SwiftUI

@main
struct Xcode_14_Essential_TrainingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
